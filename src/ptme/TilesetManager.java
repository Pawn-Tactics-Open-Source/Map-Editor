/*
    This file is part of PTME.

    PTME is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    PTME is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with PTME. If not, see <https://www.gnu.org/licenses/>.
 */

package ptme;

import util.FileSystemUtil;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TilesetManager {
  private static List<File> tilesetList = Collections.synchronizedList(new LinkedList<>());

  static {
    File currDir = FileSystemUtil.getLocalDirectory();
    File[] tilesets = currDir.listFiles((path, name) -> name.endsWith(".ptx"));
    tilesetList.addAll(Arrays.asList(tilesets));
  }


  public static File[] getTilesets() {
    return tilesetList.toArray(new File[tilesetList.size()]);
  }

  public static File getDefaultTileset() {
    File[] tilesets = getTilesets();
    for (File file : tilesets) {
      if (file.getName().equals(Config.DEFAULT_TILESET_FILE_NAME)) {
        return file;
      }
    }
    if (tilesets.length > 0) {
      return tilesets[0];
    }
    return null;
  }
}
